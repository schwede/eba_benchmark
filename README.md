# Embedding-based alignment benchmark

This repository contains the data for reproducing the benchmarks described in the paper: ‘Embedding-based alignment: combining protein language models with dynamic programming alignment to detect structural similarities in the twilight-zone’.
